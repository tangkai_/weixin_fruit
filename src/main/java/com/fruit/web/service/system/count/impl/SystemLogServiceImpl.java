package com.fruit.web.service.system.count.impl;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fruit.core.persistence.convention.BaseDao;
import com.fruit.web.mapper.system.count.SystemLogMapper;
import com.fruit.web.model.SystemLog;
import com.fruit.web.page.DataSet;
import com.fruit.web.page.PageCriterias;
import com.fruit.web.service.system.count.SystemLogService;

@Service
public class SystemLogServiceImpl extends BaseDao<SystemLog, Integer> implements SystemLogService {

	@Autowired
	private SystemLogMapper systemLogMapper;
	
	@Override
	public void cleanLogByDate() {
		// come on
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -5);
		Calendar c2 = Calendar.getInstance();
		c2.add(Calendar.DAY_OF_MONTH, -2);
		systemLogMapper.cleanLogByDate(format(c.getTime()), format(c2.getTime()));
	}
	
	private String format(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	@Override
	public DataSet<SystemLog> list(PageCriterias pcs) {
		List<SystemLog> rows = systemLogMapper.list(getParams(pcs));
		long count = systemLogMapper.listCount(getParams(pcs));
		DataSet<SystemLog> dataSet = new DataSet<SystemLog>(rows, count, count);
		return dataSet;
	}
	
	private Map<String,Object> getParams(PageCriterias pcs){
		Map<String, Object> params = new HashMap<String, Object>();
		if(null != pcs ) {
			params.put("offset", pcs.getStart());
			params.put("pagesize", pcs.getLength());
			params.put("search", pcs.getSearchValue());  //查询输入框的值
			
			//String column = pcs.getOrderColumn();
			String order = pcs.getOrderValue();
			String field = pcs.getOrderField();
			if("0".equals(field)) {
				params.put("sortName", " first_name "+order); //排序字段值
			}else if("1".equals(field)) {
				params.put("sortName", " last_name "+order);
			}else if("2".equals(field)) {
				params.put("sortName", " position "+order);
			}else if("3".equals(field)) {
				params.put("sortName", " office "+order);
			}else if("4".equals(field)) {
				params.put("sortName", " start_date "+order);
			}else if("5".equals(field)) {
				params.put("sortName", " age "+order);
			}else if("6".equals(field)) {
				params.put("sortName", " salary "+order);
			}else {
				params.put("sortName", " id "+"desc");
			}
		}
		return params;
	}

}
