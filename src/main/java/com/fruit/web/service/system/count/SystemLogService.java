package com.fruit.web.service.system.count;

import com.fruit.core.persistence.IBaseDao;
import com.fruit.web.model.SystemLog;
import com.fruit.web.page.DataSet;
import com.fruit.web.page.PageCriterias;

public interface SystemLogService extends IBaseDao<SystemLog, Integer> {

	public void cleanLogByDate();

	public DataSet<SystemLog> list(PageCriterias pcs);

}
