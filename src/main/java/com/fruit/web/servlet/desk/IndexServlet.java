package com.fruit.web.servlet.desk;


import com.fruit.web.servlet.Controller;


public class IndexServlet extends Controller {

	private static final long serialVersionUID = -3428108481045100994L;

	public void index() {
		render("index");
	}
}
