package com.fruit.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Controller extends HttpServlet {

	private static final long serialVersionUID = -7771767767658404610L;
	
	HttpServletRequest request;
	HttpServletResponse response;
	
	public String getPara(String name) {
		return request.getParameter(name);
	}
	
	public void render(String view) {
		try {
			request.getRequestDispatcher("/desk/" + view + ".jsp").forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void init(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}
}
