package com.fruit.web.servlet.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fruit.web.servlet.Controller;
import com.fruit.web.servlet.aop.Invocation;
import com.fruit.web.servlet.handler.Handler;


final class ActionHandler extends Handler {

	private final ActionMapping actionMapping;

	public ActionHandler(ActionMapping actionMapping) {
		this.actionMapping = actionMapping;
	}

	public final void handle(String target, HttpServletRequest request, HttpServletResponse response,
			boolean[] isHandled) {
		if (target.indexOf('.') != -1) {
			return;
		}
		isHandled[0] = true;
		String[] urlPara = { null };
		Action action = actionMapping.getAction(target, urlPara);

		try {
			Controller controller = (Controller) action.getControllerClass().newInstance();
			controller.init(request, response);
			new Invocation(action, controller).invoke();
		} catch (Exception e) {
		}
	}
}
