package com.fruit.web.servlet.core;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import com.fruit.web.servlet.handler.Handlers;

public class Config {

	private static final Handlers handlers = new Handlers();
	private static final Routes routes = new Routes(){public void config() {}};
	
	static void configWeb(ServletContext context) {
		Map<String, ? extends ServletRegistration> sr = context.getServletRegistrations();
		for(Entry<String, ? extends ServletRegistration> m : sr.entrySet()) {
			if(!m.getValue().getMappings().isEmpty()) {
				try {
					Class cls = Class.forName(m.getValue().getClassName());
					routes.add(m.getValue().getMappings().toArray()[0].toString(), cls);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static Routes getRoutes() {
		return routes;
	}
	
	public static Handlers getHandlers() {
		return handlers;
	}
}
