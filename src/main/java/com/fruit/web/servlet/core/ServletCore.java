package com.fruit.web.servlet.core;

import javax.servlet.ServletContext;

import com.fruit.web.servlet.handler.Handler;
import com.fruit.web.servlet.handler.HandlerFactory;

public class ServletCore {

	private Handler handler;
	private ActionMapping actionMapping;
	private static ServletCore  servletCore = new ServletCore() ;
	private ServletCore() {}
	
	public Handler getHandler() {
		return handler;
	}
	
	public static ServletCore getInstances() {
		return servletCore;
	}

	public void init(ServletContext servletContext) {
		Config.configWeb(servletContext);
		initActionMapping();
		initHandler();
	}

	private void initActionMapping() {
		actionMapping = new ActionMapping(Config.getRoutes());
		actionMapping.buildActionMapping();
	}
	
	private void initHandler() {
		Handler actionHandler = new ActionHandler(actionMapping);
		handler = HandlerFactory.getHandler(Config.getHandlers().getHandlerList(), actionHandler);
	}
}
