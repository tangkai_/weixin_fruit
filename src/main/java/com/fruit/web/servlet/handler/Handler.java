package com.fruit.web.servlet.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author jzx
 * @date 2016-10-27 10:27:40
 *
 */
public abstract class Handler {
	
	protected Handler nextHandler;
	
	public abstract void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled);
}


	

