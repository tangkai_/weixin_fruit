package com.fruit.web.servlet;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


import com.fruit.web.servlet.desk.IndexServlet;
import com.fruit.web.servlet.desk.about.AboutServlet;
import com.fruit.web.servlet.desk.category.CategoryServlet;
import com.fruit.web.servlet.desk.customer.CustomerServlet;
import com.fruit.web.servlet.desk.news.NewsServlet;
import com.fruit.web.servlet.desk.shopping.ShopServlet;


/**
 * @author jzx
 * @date 2016-10-26 16:55:08
 */
@WebListener
public class DeskContextListener implements ServletContextListener {

	ServletContext context;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		context = sce.getServletContext();
		context.addServlet("IndexServlet", IndexServlet.class).addMapping("/indexServlet");
		context.addServlet("AboutServlet", AboutServlet.class).addMapping("/aboutServlet");
		context.addServlet("CategoryServlet", CategoryServlet.class).addMapping("/categoryServlet");
		context.addServlet("CustomerServlet", CustomerServlet.class).addMapping("/customerServlet");
		context.addServlet("NewsServlet", NewsServlet.class).addMapping("/newsServlet");
		context.addServlet("ShopServlet", ShopServlet.class).addMapping("/shopServlet");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

}
