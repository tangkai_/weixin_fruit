package com.fruit.web.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fruit.web.servlet.core.ServletCore;
import com.fruit.web.servlet.handler.Handler;

/**
 * 
 * @author jzx
 * @date 2016-10-27 10:28:37
 *
 *	项目参考JFinal源码实现，此处剥离一部分出来
 */
@WebFilter(urlPatterns = "/*")
public class DeskFilter implements Filter {

	private Handler handler;
	private static final ServletCore servletCore = ServletCore.getInstances();

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		servletCore.init(arg0.getServletContext());
		handler = servletCore.getHandler();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		String target = req.getRequestURI();
		boolean[] isHandled = { false };
		try {
			handler.handle(target, req, res, isHandled);
		} catch (Exception e) {
		}
		if (isHandled[0] == false)
			chain.doFilter(req, res);
	}

	@Override
	public void destroy() {
	}
}
