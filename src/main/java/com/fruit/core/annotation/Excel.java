package com.fruit.core.annotation;

public @interface Excel {

	String exportName();

}
